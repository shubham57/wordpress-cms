import { useEffect, useState } from 'react';
import React, { Component } from 'react';

function Privacy({ privacyData }) {
    const [posts, setPosts] = useState(privacyData);

    return (
        <>
            <div className="container bg">
                <div className="page-wrap ">
                    <h1 dangerouslySetInnerHTML={{ __html: posts.title.rendered }}></h1>
                    <div dangerouslySetInnerHTML={{ __html: posts.content.rendered }}></div>
                </div>
            </div>
        </>
    );
}
export default Privacy;