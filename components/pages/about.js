import { useEffect, useState } from 'react';


function About({ aboutData }) {
    const [posts, setPosts] = useState(aboutData);

    return (
        <>
            <div className="container bg">
                <div className="page-wrap ">
                    <h1 dangerouslySetInnerHTML={{ __html: posts.title.rendered }}></h1>
                    <div dangerouslySetInnerHTML={{ __html: posts.content.rendered }}></div>
                </div>
            </div>
        </>
    );
}
export default About;