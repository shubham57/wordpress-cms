import React from 'react';
import moment from 'moment';
import {decode} from 'html-entities';
export default function Banner ({post}){

  var sectionStyle = {
    background: "url("+post._embedded['wp:featuredmedia'][0]['source_url']+")",
    backgroundSize:"cover"
  };

	return(
		<><div className="card">  
        <div className="xop-box xop-img-1" style={sectionStyle}>
            <div className="xop-info">
              <p className="card-category"></p>
              <h3>{decode(post.title.rendered) }</h3>
              {/* <p>Updated {moment(post.date).format("LL")}</p> */}
            </div>
          
        </div>
      </div>
    
		</>
		)
}