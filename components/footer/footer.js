import React from 'react'

export default function Footer(props){
	return(
<footer className="footer">
  <div className="pageList">
    <a href="/about">About Us</a>
    <a href="/contact">Contact Us</a>
    <a href="/privacy">Privacy Policy</a>
  </div>
  <div>
    <i className="far fa-copyright"></i> Thedailypanda
  </div>
</footer>
		)
}