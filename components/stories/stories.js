import React from 'react';
import {decode} from "html-entities";
import Skeleton from 'react-loading-skeleton'
import 'react-loading-skeleton/dist/skeleton.css'
export default function Stories({post , loading , relatedPosts }) {
  if (loading) {
    return (
      <div className="card3">
      <div className="card-img">
      <Skeleton height={245} width={275} />
      </div>
      <div className="card-content">
        <p className="card3-category"></p>
        <p className="card-header">
        <Skeleton height={10} count={3} />
        </p>
      </div>
    </div>
    );
  }
  let pbg =
    post.featured_media === 0
      ? ""
      : post._embedded["wp:featuredmedia"][0]["source_url"];
  var sectionStyle = {
    backgroundImage: "url(" + pbg + ")",
    backgroundSize:"cover"
  };
  return (
    
    <div className="card3">
    <div className="card-img" style={sectionStyle}  ></div>
    <div className="card-content">
      <p className="card3-category"></p>
      <p className="card-header">{decode(post.title.rendered) } </p>
    </div>
  </div>
  );
}