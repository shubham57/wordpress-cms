import { decode } from "html-entities";
import Ads from "./ads/ads";
import RelatedStories from "../components/relatedStories/relatedstories";
import { useState } from "react";
import { useEffect } from "react";
import { useRouter } from "next/router";
import Link from "next/link";

export default function Content({ post, pagination, relatedPosts }) {
  const [index, setIndex] = useState(0);
  const [nextBtn, setButton] = useState(false);
  const router = useRouter();
  const changeUrl = (i) => {
    let q = nextBtn === "gal" ? "?utm_term=gal" : "";
    history.pushState(
      {},
      "",
      "/article" + "/" + router.query.slug + "/" + parseInt(i) + q
    );
  };
  useEffect(() => {
    if (router.isReady) {
      const { utm_term } = router.query;
      if (pagination === true && utm_term !== "gal") {
        document.addEventListener("scroll", function (e) {
          var buttons = document.getElementsByClassName("wp-block-group");
          for (var i = 0; i < buttons.length; i++) {
            var placement = buttons[i].getBoundingClientRect();
            if (placement.top < window.innerHeight && placement.bottom > 0) {
              // history.pushState({}, '', i + 1);
              changeUrl(i + 1);
              setIndex(i + 1);
              return false;
            }
          }
        });
      }
      setButton(utm_term);
      if (pagination === true && utm_term === "gal") {
        document.removeEventListener("scroll", function () {
          // console.log("removed");
          return false;
        });
        DisplayIndexArticle(0);
      }
    }
  }, [router.isReady]);
  const DisplayIndexArticle = (ind) => {
    var blocks = document.getElementsByClassName("wp-block-group");
    for (let i = 0; i < blocks.length; i++) {
      blocks[i].style.display = i === ind ? "block" : "none";
    }
  };
  const changeDisplay = (action) => {
    var blocks = document.getElementsByClassName("wp-block-group");
    if (action === "+" && blocks.length - 1 > index) {
      DisplayIndexArticle(index + 1);
      changeUrl(index + 1);
      setIndex(index + 1);
    } else if (action === "-") {
      if (index > 0) {
        DisplayIndexArticle(index - 1);
        //  let ins = index - 1;
        changeUrl(index - 1);
        setIndex(index - 1);
      }
    }
  };
  // }

  return (
    <div>
      <div className="container row pd-top">
        <div className="article-content leftcolumn">
          <div className="card2">
            <div className="">
              {/* {post} */}
              <div
                dangerouslySetInnerHTML={{
                  __html: `
              `,
                }}
              ></div>
              <h1 className="card-title">{decode(post[0].title.rendered)} </h1>
              <div
                dangerouslySetInnerHTML={{
                  __html: decode(post[0].content.rendered),
                }}
              ></div>
              {post[0].acf.item != null && post[0].acf.item.length > 0 ? (
                post[0].acf.item.map((block, index) => {
                  return (
                    <div className="wp-block-group">
                      <h2>{block.name}</h2>
                      {/* Ads here */}
                      <div
                        dangerouslySetInnerHTML={{ __html: block.image }}
                      ></div>
                      {/* Ads here */}
                      <div
                        dangerouslySetInnerHTML={{ __html: block.features }}
                      ></div>
                    </div>
                  );
                })
              ) : (
                <></>
              )}
              {nextBtn === "gal" ? (
                <>
                  <button
                    class="button-class"
                    onClick={() => {
                      changeDisplay("-");
                    }}
                  >
                    Prev
                  </button>
                  <button
                    class="button-class"
                    onClick={() => {
                      changeDisplay("+");
                    }}
                  >
                    Next
                  </button>
                </>
              ) : (
                <></>
              )}
            </div>
          </div>
          <div className="related-data">
            <h1>Related Stories</h1>
            <div className="lowerSection">
              {relatedPosts.slice(1, 4).map((ps) => (
                <Link key={ps.id} href={ps.slug}>
                  <a>
                    <RelatedStories relatedPosts={ps}></RelatedStories>
                  </a>
                </Link>
              ))}
            </div>
          </div>
        </div>
        <div className="rightcolumn ">
          {index >= 0 ? <Ads index={index} /> : <></>}
        </div>
      </div>
    </div>
  );
}
