import React from 'react';
import Link from 'next/link'

export default function Header(props) {
  return (
    <>
  <div className="header">
     <div className="head-container">
         <Link href="/"><a className="logo"><img src="/logo.png" alt="logo"/></a></Link>
      </div>
  </div>

    </>
  )
}
