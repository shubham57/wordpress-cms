import Document, { Html, Head, Main, NextScript } from 'next/document'

class MyDocument extends Document {
  static async getInitialProps(ctx) {
    const initialProps = await Document.getInitialProps(ctx)
    return { ...initialProps }
  }

  render() {
    return (
      <Html>
          
        <Head>
        <meta charset="utf-8" />
          <link href="https://fonts.googleapis.com/css?family=Open+Sans|Oswald|Fugaz+One|PT+Serif|Vollkorn" rel="stylesheet" />
          <div dangerouslySetInnerHTML={{ __html: `
          <script async src="https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js?client=ca-pub-4235796132533153"
          crossorigin="anonymous"></script>
          <script async src="https://securepubads.g.doubleclick.net/tag/js/gpt.js"></script>
          <script>
  var gptadslots = [];

  var googletag = googletag || {cmd:[]};

</script>
       
          <script type="text/javascript" src="//fdyn.pubwise.io/script/46f5f890-9605-4b0f-90df-dddbd53589f2/v3/dyn/pre_pws.js?type=default"></script>
          <script type="text/javascript" src="//fdyn.pubwise.io/script/46f5f890-9605-4b0f-90df-dddbd53589f2/v3/dyn/pws.js?type=default" async></script>
          <script>
            googletag.cmd.push(function() {
            var mapping1= googletag.sizeMapping()
            .addSize([1024, 0], [[970,250],[970,90],[728,90]])
            .addSize([500, 0], [[728,90]])
            .addSize([0, 0], ['fluid',[300,250]])
            .build();
            var mapping2= googletag.sizeMapping()
            .addSize([1024, 0], [[300,250]])
            .addSize([500, 0], [[300,250]])
            .addSize([0, 0], [[300,250]])
            .build();
            var mapping3= googletag.sizeMapping()
            .addSize([1024, 0], ['fluid',[300,250],[300,600],[160,600]])
            .addSize([500, 0], [,[300,250],[160,600]])
            .addSize([0, 0], [,[300,250]])
            .build();


            gptadslots['tdpanda_leaderboard1'] = googletag.defineSlot('/21700304385,22502190755/thedailypanda/tdpanda_leaderboard1', [728, 90], 'tdpanda_leaderboard1')
            .defineSizeMapping(mapping1)
            .addService(googletag.pubads());

            gptadslots['tdpanda_inarticle1'] = googletag.defineSlot('/21700304385,22502190755/thedailypanda/tdpanda_inarticle1', [300, 250], 'tdpanda_inarticle1')
            .defineSizeMapping(mapping2)
            .addService(googletag.pubads());

            gptadslots['tdpanda_inarticle2'] = googletag.defineSlot('/21700304385,22502190755/thedailypanda/tdpanda_inarticle2', [300, 250], 'tdpanda_inarticle2')
            .defineSizeMapping(mapping2)
            .addService(googletag.pubads());

            gptadslots['tdpanda_rail_top'] = googletag.defineSlot('/21700304385,22502190755/thedailypanda/tdpanda_rail_top', [300, 250], 'tdpanda_rail_top')
            .defineSizeMapping(mapping3)
            .addService(googletag.pubads());

            gptadslots['tdpanda_rail_mid_1'] = googletag.defineSlot('/21700304385,22502190755/thedailypanda/tdpanda_rail_mid_1', [300,250], 'tdpanda_rail_mid_1')
            .defineSizeMapping(mapping3)
            .addService(googletag.pubads());

            gptadslots['tdpanda_rail_mid_2'] = googletag.defineSlot('/21700304385,22502190755/thedailypanda/tdpanda_rail_mid_2', [300,250], 'tdpanda_rail_mid_2')
            .defineSizeMapping(mapping2)
            .addService(googletag.pubads());

            gptadslots['tdpanda_rail_mid_3'] = googletag.defineSlot('/21700304385,22502190755/thedailypanda/tdpanda_rail_mid_3', [300, 250], 'tdpanda_rail_mid_3')
            .defineSizeMapping(mapping2)
            .addService(googletag.pubads());
            gptadslots['interstitial'] = googletag.defineOutOfPageSlot('/21700304385,22502190755/thedailypanda/tdpanda_interstitial', googletag.enums.OutOfPageFormat.INTERSTITIAL).addService(googletag.pubads());
            googletag.pubads().set('page_url', 'https://www.thedailypanda.com/');
            googletag.pubads().setCentering(true);
            googletag.enableServices();
            });
          </script>
          <script async src="https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js?client=ca-pub-1829670945164796"
     crossorigin="anonymous"></script>
          `}}></div>
        </Head>
        <body>
          <Main />
          <NextScript />

          <div dangerouslySetInnerHTML={{ __html: `      
          `}}></div>
          
        </body>
      </Html>
    )
  }
}

export default MyDocument