import React from 'react';
import Header from '../components/header/header';
import Privacy from '../components/pages/privacy';
import Footer from '../components/footer/footer';
export default function privacyPage({ privacyJson }) {

    return (
        <React.StrictMode>
            <Header></Header>
            <Privacy privacyData={privacyJson} />
            <Footer></Footer>
        </React.StrictMode>

    )
}
export async function getStaticProps() {
    let postData = await fetch('https://api.thedailypanda.com/wp-json/wp/v2/pages/3');
    let privacyJson = await postData.json();
    return { props: { privacyJson } }
}