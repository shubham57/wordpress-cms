import '../styles/globals.css'
import '../components/header/header.css';
import '../components/banner/banner.css'
import '../components/footer/footer.css'
import '../components/artical/artical.css'
import Head from "next/head";
function MyApp({ Component, pageProps }) {

  return (
    <>
      <Head>
        <script async src='https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js?client=ca-pub-1829670945164796'
          crossorigin='anonymous'></script>
      </Head>
      <Component {...pageProps} />
    </>
  );
}

export default MyApp

