import React from 'react';
import Header from '../components/header/header';
import Contact from '../components/pages/contact';
import Footer from '../components/footer/footer';
export default function contactPage({ contactJson }) {

    return (

        <React.StrictMode>
            <Header></Header>
            <Contact contactData={contactJson} />
            <Footer></Footer>
        </React.StrictMode>

    )
}
export async function getStaticProps() {
    let postData = await fetch('https://api.thedailypanda.com/wp-json/wp/v2/pages/140');
    let contactJson = await postData.json();
    return { props: { contactJson } }
}