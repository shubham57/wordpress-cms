
import React from 'react';
import Header from '../components/header/header';
import About from '../components/pages/about';
import Footer from '../components/footer/footer';
export default function AboutPage({ aboutJson }) {
    return (
        <React.StrictMode>
            <Header />
            <About aboutData={aboutJson} />
            <Footer></Footer>
        </React.StrictMode>
    )
}
export async function getStaticProps() {
    let postData = await fetch('https://api.thedailypanda.com/wp-json/wp/v2/pages/109');
    let aboutJson = await postData.json();
    return { props: { aboutJson } }
}