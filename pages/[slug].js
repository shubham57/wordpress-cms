
import React from 'react';
import Header from '../components/header/header'
import Footer from '../components/footer/footer'
import Content from '../components/content';
export default function blog({ post , relatedPosts }) {
    return (
        <React.StrictMode>
           <Header/>
            <Content post={post} relatedPosts={relatedPosts} />
        <Footer/>
        </React.StrictMode>
    )
}

// This function gets called at build time
export async function getStaticPaths() {
    let flag=1;
    let i=1;
    let pages=[];
    while (flag==1) {
        try{
            let postData = await fetch('https://api.thedailypanda.com/wp-json/wp/v2/posts?_fields=id,slug,title&tags_exclude=4&per_page=10&status=publish&page='+i);
            let SitemapJson = await postData.json();
            if(SitemapJson!==[]){
                pages.push(...SitemapJson);
                i++;
            }
        }catch(err){
            flag=0;
        }
    }
    const paths = pages.map((post) => ({
        params: { slug: post.slug },
    }))
    return { paths, fallback: false }
}

export async function getStaticProps({ params }) {
    const res = await fetch(`https://api.thedailypanda.com/wp-json/wp/v2/posts?tags_exclude=4&_embed&slug[]=${params.slug}`)
    let post = await res.json()
    let relatedPosts = await getRelatedProducts(post[0].id);    
    return { props: { post , relatedPosts } }
}
let getRelatedProducts = (postId) => {
    return  new Promise(function (resolve, reject) {
        try {
            fetch('https://api.thedailypanda.com/wp-json/yarpp/v1/related/' + postId +"?_embed&tags_exclude=4&per_page=3")
                .then((data) => {
                    return data.json();
                })
                .then((data) => {
                    resolve(data);
                })
        } catch {
            reject([]);
        }
        });
}
