import Header from '../components/header/header'
import Banner from '../components/banner/banner'
import Stories from '../components/stories/stories'
import Ads from '../components/ads/ads'
import Footer from '../components/footer/footer'
import axios from "axios";
import React, { useState, useEffect } from "react";
import Link from 'next/link'

export default function Home({ posts }) {
  const featurePosts = posts.slice(0, 3);
  const listPosts = posts.slice(4, 10);
  const [pages, setPages] = useState(1);
  const [data, setData] = useState(posts);
  const [loading, setLoading] = useState(true);
  const [totalPg, setTotalPg] = useState();
  const changePage = (val) => {
    if (val > totalPg || val < 1) {
      setPages(1);
    } else {
      setPages(val);
    }
  };
  useEffect(() => {
    async function fetchData() {
      setLoading(true);
      const res = await axios(
        "https://api.thedailypanda.com/wp-json/wp/v2/posts?&_embed&tags_exclude=4&per_page=6&page=" +
          pages
      );
      setTotalPg(parseInt(res.headers["x-wp-totalpages"]));
      setData(res.data);
      setLoading(false);
    }
    fetchData();
  }, [pages]);
  return (
    <>
      <Header />
      <div className="container">
        <div className="cards">
          {featurePosts.map((fp , index) => {
            return <Link key={index} href={fp.slug}><a><Banner post={fp} /></a></Link>
          })}
        </div>
      </div>
      <div className="container row">
        <div className="leftcolumn">
          <div className="card2">
            <h1>Trending Stories</h1>
            {loading ? (
              <>
                <div className="lowerSection">
                  {[0,1,2,3,4,5].map((ak)=>(
                    <Stories loading={loading} />
                  ))}
                </div>
              </>
            ) : (
              <>
                <div className="lowerSection">
                  {data.map((ps) => (
                    <Link key={ps.id} href={ps.slug}><a>
                    <Stories post={ps} loading={loading} /></a>
                  </Link>
                  ))}
                </div>
              </>
            )}
            <div className=" data-pagination">
              {pages > totalPg - 3 ? (
                <>
                  <ul>
                    <li
                      id="prev-btn"
                      className={pages === 1 ? "disabled" : ""}
                      onClick={() => {
                        changePage(pages - 1);
                      }}
                    >
                      &#60;&#60;
                    </li>
                    <li
                      onClick={() => {
                        changePage(1);
                      }}
                    >
                      {1}
                    </li>
                    <li
                      onClick={() => {
                        changePage(2);
                      }}
                    >
                      {2}
                    </li>
                    <li>...</li>
                    <li
                      className={pages === totalPg - 2 ? "current" : ""}
                      onClick={() => {
                        changePage(totalPg - 2);
                      }}
                    >
                      {totalPg - 2}
                    </li>
                    <li
                      className={pages === totalPg - 1 ? "current" : ""}
                      onClick={() => {
                        changePage(totalPg - 1);
                      }}
                    >
                      {totalPg - 1}
                    </li>
                    <li
                      className={pages === totalPg ? "current" : ""}
                      onClick={() => {
                        changePage(totalPg);
                      }}
                    >
                      {totalPg}
                    </li>
                    <li
                      id="next-btn"
                      className={pages === totalPg ? "disabled" : ""}
                      onClick={() => {
                        changePage(pages + 1);
                      }}
                    >
                      &#62;&#62;
                    </li>
                  </ul>
                </>
              ) : (
                <>
                  <ul>
                    <li
                      id="prev-btn"
                      className={pages === 1 ? "disabled" : ""}
                      onClick={() => {
                        changePage(pages - 1);
                      }}
                    >
                      &#60;&#60;
                    </li>
                    <li
                      className={pages === totalPg ? "" : "current"}
                      onClick={() => {
                        changePage(0 + pages);
                      }}
                    >
                      {0 + pages}
                    </li>
                    <li
                      onClick={() => {
                        changePage(1 + pages);
                      }}
                    >
                      {1 + pages}
                    </li>
                    <li
                      onClick={() => {
                        changePage(2 + pages);
                      }}
                    >
                      {2 + pages}
                    </li>
                    <li
                      onClick={() => {
                        changePage(3 + pages);
                      }}
                    >
                      {3 + pages}
                    </li>
                    <li>...</li>
                    <li
                      className={pages === totalPg ? "current" : ""}
                      onClick={() => {
                        changePage(totalPg);
                      }}
                    >
                      {totalPg}
                    </li>
                    <li
                      id="next-btn"
                      className={pages === totalPg ? "disabled" : ""}
                      onClick={() => {
                        changePage(pages + 1);
                      }}
                    >
                      &#62;&#62;
                    </li>
                  </ul>{" "}
                </>
              )}
            </div>
          </div>
        </div>
        <div className="rightcolumn">
          <Ads />
        </div>
      </div>
      <Footer />
    </>
  )
}

export async function getStaticProps({ params }) {
  const res = await fetch(`https://api.thedailypanda.com/wp-json/wp/v2/posts?tags_exclude=4&_embed&per_page=20&exclude=`)
  let posts = await res.json();
  return { props: { posts } }
}